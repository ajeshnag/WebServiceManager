package com.ajeshnag.webservicemanager;

import android.content.Context;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.HashMap;
import java.util.Map;

/**
 * // TODO polling
 * Created by Ajesh Nag on 03/01/16.
 */
public abstract class BaseRequest<T> extends GsonRequest<T> {

    private static String sBaseUrl = "*Base Url*";

    private static final HashMap<String, String> EMPTY_HEADERS = new HashMap<>();

    public BaseRequest(Context context, int requestType, String url, Class<T> clazz, final IResponse<T> responseCallback) {

        this(context, requestType, url, clazz, EMPTY_HEADERS, responseCallback);
    }

    public BaseRequest(Context context, int requestType, String url, Class<T> clazz, Map<String, String> headers, final IResponse<T> responseCallback) {

        super(context, requestType, url, clazz, headers, new StatusResponse<T>() {
            @Override
            public void onResponse(int httpStatusCode, T response) {

                if (responseCallback == null) {
                    return;
                }

                responseCallback.onResponse(httpStatusCode, response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                responseCallback.onError(volleyError);
            }
        });
    }

    public static String getBaseUrl() {

        return sBaseUrl;
    }

    public static void setBaseUrl(String baseUrl) {

        sBaseUrl = baseUrl;
    }

    protected static String getUrl(String path, String... params) {

        if (params.length == 0) {
            return "";
        }

        String url = sBaseUrl + "/" + path;
        for (String param : params) {
            url += "/" + param;
        }

        return url;
    }

   protected static String getUrl(String path) {

        return getBaseUrl() + "/" + path;
    }
}
