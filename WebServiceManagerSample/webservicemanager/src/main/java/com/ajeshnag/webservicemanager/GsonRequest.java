package com.ajeshnag.webservicemanager;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * Custom generic {@link Request} that parses network response into a Gson model class.
 * <p/>
 * Created by Ajesh Nag on 03/01/16.
 */
public class GsonRequest<T> extends Request<T> {

    private static final String TAG = GsonRequest.class.getSimpleName();

    private Context mContext;
    private final Gson mGson = new Gson();
    private final Class<T> mClass;
    private final Map<String, String> mHeaders;
    private final StatusResponse<T> mStatusResponse;

    private int mStatusCode;

    public GsonRequest(Context context, int requestType, String url, Class<T> clazz, Map<String, String> headers,
                       StatusResponse<T> statusResponse, Response.ErrorListener errorListener) {

        super(requestType, url, errorListener);
        mContext = context;
        mClass = clazz;
        mHeaders = headers;
        mStatusResponse = statusResponse;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {

        return mHeaders != null ? mHeaders : super.getHeaders();
    }

    @Override
    protected void deliverResponse(T response) {

        mStatusResponse.onResponse(mStatusCode, response);
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {

        mStatusCode = response.statusCode;
        try {
            String json = new String(
                    response.data,
                    HttpHeaderParser.parseCharset(response.headers));
            return Response.success(
                    mGson.fromJson(json, mClass),
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        }
    }

    public void start() {

        Log.i(TAG, "Starting request with tag - " + this.getTag());
        RequestManager.getInstance(mContext).addToRequestQueue(this);
    }
}