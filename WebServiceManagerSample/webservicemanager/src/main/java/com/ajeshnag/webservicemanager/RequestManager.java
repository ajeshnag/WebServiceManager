package com.ajeshnag.webservicemanager;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

/**
 * Singleton that maintains a {@link RequestQueue} used across the app.
 * <p/>
 * Created by Ajesh Nag on 04/01/16.
 */
public class RequestManager {

    private static final int CACHE_SIZE = 20;

    private static RequestManager sInstance;
    private static Context sContext;

    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;

    public static synchronized RequestManager getInstance(Context context) {

        if (sInstance == null) {
            sInstance = new RequestManager(context);
        }

        return sInstance;
    }

    private RequestManager(Context context) {

        sContext = context;
        mRequestQueue = getRequestQueue();

        mImageLoader = new ImageLoader(mRequestQueue,
                new ImageLoader.ImageCache() {
                    private final LruCache<String, Bitmap>
                            cache = new LruCache<>(CACHE_SIZE);

                    @Override
                    public Bitmap getBitmap(String url) {

                        return cache.get(url);
                    }

                    @Override
                    public void putBitmap(String url, Bitmap bitmap) {

                        cache.put(url, bitmap);
                    }
                });
    }

    public RequestQueue getRequestQueue() {

        if (mRequestQueue == null) {
            // use Application Context to avoid leaks
            mRequestQueue = Volley.newRequestQueue(sContext);
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(GsonRequest<T> request) {

        getRequestQueue().add(request);
    }

    public ImageLoader getImageLoader() {

        return mImageLoader;
    }
}
