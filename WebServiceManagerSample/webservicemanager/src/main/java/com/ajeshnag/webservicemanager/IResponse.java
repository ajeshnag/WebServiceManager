package com.ajeshnag.webservicemanager;

import com.android.volley.VolleyError;

/**
 * Created by Ajesh Nag on 14/01/16.
 */
public interface IResponse<T> {

    void onResponse(int httpResponseCode, T data);

    void onError(VolleyError error);
}
