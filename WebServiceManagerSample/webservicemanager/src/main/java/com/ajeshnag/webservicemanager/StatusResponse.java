package com.ajeshnag.webservicemanager;

/**
 * Created by Ajesh Nag on 14/01/16.
 */
public interface StatusResponse<T> {

    /**
     * Called when a response is received.
     */
    void onResponse(int httpStatusCode, T response);
}
