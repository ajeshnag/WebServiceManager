package com.ajeshnag.webservicemanager;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;

/**
 * Singleton that abstracts HTTP requests.
 * <p/>
 * <b>Convention:</b>*HTTPRequestType*Data(Params...)
 * <pre>
 * e.g. getXXX(Object...)
 * </pre>
 * <p/>
 * Created by Ajesh Nag on 10/12/15.
 */
public class WebServiceManager {

    private static WebServiceManager sInstance;

    protected RequestManager mRequestManager;

    public RequestManager getRequestManager() {

        return mRequestManager;
    }

    /*===================
        Initialization
     ====================*/

    /**
     * Initialize the WebServiceManager.
     *
     * @param context Application Context
     * @param baseUrl of the WebService.
     * @return {@link WebServiceManager} singleton instance
     */
    public static synchronized WebServiceManager init(Context context, String baseUrl) {

        if (sInstance == null) {
            sInstance = new WebServiceManager(context, baseUrl);
        }

        return sInstance;
    }

    public static synchronized WebServiceManager getInstance() {

        if (sInstance == null) {
            throw new IllegalArgumentException("Must call WebServiceManager#getInstance(context, baseUrl) before calling WebServiceManager#getInstance()");
        }

        return sInstance;
    }

    public WebServiceManager(Context context, String baseUrl) {

        mRequestManager = RequestManager.getInstance(context);
        BaseRequest.setBaseUrl(baseUrl);
    }

    public RequestQueue getRequestQueue() {

        return mRequestManager.getRequestQueue();
    }

    public <T> void addToRequestQueue(GsonRequest<T> request) {

        getRequestQueue().add(request);
    }

    public ImageLoader getImageLoader() {

        return getRequestManager().getImageLoader();
    }
}
