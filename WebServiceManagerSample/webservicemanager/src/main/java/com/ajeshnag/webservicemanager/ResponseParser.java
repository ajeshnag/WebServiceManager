package com.ajeshnag.webservicemanager;

/**
 * Created by Ajesh Nag on 20/01/16.
 */
public class ResponseParser {

    public enum ResponseType {

        INFORMATIONAL,
        SUCCESSFUL,
        REDIRECTION,
        CLIENT_ERROR,
        SERVER_ERROR,
        BAD_SERVER_RESPONSE
    }

    public static ResponseType parse(int responseCode) {

        switch (responseCode / 100) {

            case 1:
                return ResponseType.INFORMATIONAL;
            case 2:
                return ResponseType.SUCCESSFUL;
            case 3:
                return ResponseType.REDIRECTION;
            case 4:
                return ResponseType.CLIENT_ERROR;
            case 5:
                return ResponseType.SERVER_ERROR;
            default:
                return ResponseType.BAD_SERVER_RESPONSE;
        }
    }
}
