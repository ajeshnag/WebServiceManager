package com.ajeshnag.webservicemanagersample;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.ajeshnag.webservicemanager.IResponse;
import com.ajeshnag.webservicemanager.ResponseParser;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import java.lang.reflect.Field;

import manager.ProjectWebServiceManager;
import models.SampleModel;
import requests.SampleGetRequest;

public class MainActivity extends AppCompatActivity {

    private TextView mStatusText;

    private Gson mGson;

    private ProjectWebServiceManager mProjectWebServiceManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        mProjectWebServiceManager = ProjectWebServiceManager.init(this.getApplicationContext(), getString(R.string.web_services_base_url));

        mGson = new Gson();

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mStatusText = (TextView) findViewById(R.id.status_text);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(mTestRequestClickListener);
    }

    private View.OnClickListener mTestRequestClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            mStatusText.setTextColor(getResources().getColor(android.R.color.black));
            setStatusText("Executing test request...");

            final int intParam = 999;
            final String stringParam = "stringParam";

            requestSampleWSM(intParam, stringParam);
            //requestSample(intParam, stringParam);
        }
    };

    private void setStatusText(String status) {

        mStatusText.setText(status);
    }

    private void setStatusText(String prefix, int httpResponseCode, String body, boolean success) {

        mStatusText.setText(prefix + " " + httpResponseCode + " " + ResponseParser.parse(httpResponseCode) + "\n\n" + body);

        mStatusText.setTextColor(getResources().getColor((success ? android.R.color.holo_green_dark : android.R.color.holo_red_dark)));
    }

    private void setStatusObject(Object object) {

        mStatusText.setText(getObjectString(object));
    }

    private String getObjectString(Object object) {

        String objectString = "";
        for (Field field : object.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            String name = field.getName();
            Object value = null;
            try {
                value = field.get(object);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

            objectString += name + " : " + value + "\n";
        }

        return objectString;
    }

    private String getJson(Object jsonObject) {

        return mGson.toJson(jsonObject);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /*============
        Requests
     =============*/

    /**
     * Start the request directly
     */
    private void requestSample(int intParam, String stringParam) {

        SampleGetRequest sampleGetRequest = SampleGetRequest.create(MainActivity.this, intParam, stringParam, new IResponse<SampleModel>() {
            @Override
            public void onResponse(int httpResponseCode, SampleModel data) {

                setStatusText("Response:", httpResponseCode, getJson(data), true);
            }

            @Override
            public void onError(VolleyError error) {

                setStatusText("Error:", error.networkResponse.statusCode, getJson(error), false);
            }
        });

        sampleGetRequest.start();
    }

    /**
     * Start the request using the WebServiceManager
     */
    private void requestSampleWSM(int intParam, String stringParam) {

        mProjectWebServiceManager.getSample(MainActivity.this, 999, "testStringParam", new IResponse<SampleModel>() {
            @Override
            public void onResponse(int httpResponseCode, SampleModel data) {

                setStatusText("Response:", httpResponseCode, getJson(data), true);
            }

            @Override
            public void onError(VolleyError error) {

                setStatusText("Error:", (error != null) && (error.networkResponse != null) ? error.networkResponse.statusCode : -1, getJson(error), false);
            }
        });
    }
}
