package manager;

import android.content.Context;

import com.ajeshnag.webservicemanager.BaseRequest;
import com.ajeshnag.webservicemanager.IResponse;
import com.ajeshnag.webservicemanager.RequestManager;
import com.ajeshnag.webservicemanager.WebServiceManager;

import models.SampleModel;
import requests.SampleGetRequest;
import requests.SamplePostRequest;

/**
 * Project specific {@link WebServiceManager}.
 * <p/>
 * Created by Ajesh Nag on 19/01/16.
 */
public class ProjectWebServiceManager extends WebServiceManager {

    private static ProjectWebServiceManager sInstance;

    /*===================
        Initialization
     ====================*/

    /**
     * Initialize the WebServiceManager.
     *
     * @param context Application Context
     * @param baseUrl of the WebService.
     * @return {@link ProjectWebServiceManager} singleton instance
     */
    public static synchronized ProjectWebServiceManager init(Context context, String baseUrl) {

        if (sInstance == null) {
            sInstance = new ProjectWebServiceManager(context, baseUrl);
        }

        return sInstance;
    }

    public static synchronized ProjectWebServiceManager getInstance() {

        if (sInstance == null) {
            throw new IllegalArgumentException("Must call WebServiceManager#getInstance(context, baseUrl) before calling WebServiceManager#getInstance()");
        }

        return sInstance;
    }

    private ProjectWebServiceManager(Context context, String baseUrl) {

        super(context, baseUrl);

        mRequestManager = RequestManager.getInstance(context);
        BaseRequest.setBaseUrl(baseUrl);
    }

    /*===================
        Request Methods
     ===================*/

    public void getSample(Context context, int intParam, String stringParam, IResponse<SampleModel> response) {

        SampleGetRequest sampleGetRequest = SampleGetRequest.create(context, intParam, stringParam, response);
        sampleGetRequest.start();
    }

    public void postSample(Context context, int intParam, String stringParam, IResponse<SampleModel> response) {

        SamplePostRequest samplePostRequest = SamplePostRequest.create(context, intParam, stringParam, response);
        samplePostRequest.start();
    }
}
