package models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ajesh Nag on 14/01/16.
 */
public class SampleModel {

    public Args args;

    public static class Args {

    }

    public Headers headers;

    public static class Headers {

        @SerializedName("Accept")
        public String accept;

        @SerializedName("Accept-Encoding")
        public String acceptEncoding;

        @SerializedName("Accept-Language")
        public String acceptLanguage;

        @SerializedName("Cookie")
        public String cookie;

        @SerializedName("Host")
        public String host;

        @SerializedName("User-Agent")
        public String userAgent;
    }

    public String origin;
    public String url;
}
