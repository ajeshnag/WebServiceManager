package requests;

import android.content.Context;

import com.ajeshnag.webservicemanager.BaseRequest;
import com.ajeshnag.webservicemanager.IResponse;

import java.util.HashMap;

import models.SampleModel;

/**
 * Created by Ajesh Nag on 14/01/16.
 */
public class SampleGetRequest extends BaseRequest<SampleModel> {

    private static final String PARAM_TEST_INT = "test_int";
    private static final String PARAM_TEST_STRING = "test_string";

    public static SampleGetRequest create(Context context, int intParam, String stringParam, IResponse<SampleModel> listener) {

        return new SampleGetRequest(context, Method.GET, intParam, stringParam, listener);
    }

    private SampleGetRequest(Context context, int requestType, int intParam, String stringParam, IResponse<SampleModel> listener) {

        super(context, requestType, getUrl(intParam, stringParam), SampleModel.class, getHeaders(intParam, stringParam), listener);
    }

    private static String getUrl(int intParam, String stringParam) {

        return BaseRequest.getUrl("get");
    }

    private static HashMap<String, String> getHeaders(int intParam, String stringParam) {

        HashMap<String, String> headers = new HashMap<>();
        headers.put(PARAM_TEST_INT, "" + intParam);
        headers.put(PARAM_TEST_STRING, stringParam);

        return headers;
    }
}
